---
layout: talk
title: Resolving gas temperature distributions with single-beam dual-comb absorption spectroscopy
authors: NA Malarich, GB Rieker
venue: San Jose
event: CLEO
eventlong: Conference on Lasers and Electro-Optics
era: PhD
project: nonuniformity
year: 2017
doi: 10.1364/CLEO_SI.2017.SW1L.7
---

## Abstract

We assess the potential to resolve line-of-sight gas temperature distributions with single-beam, broadband, dual-comb absorption spectroscopy. 
The technique shows promise for single-optical-port, spatially-resolved temperature diagnostics for harsh environments.

## Retrospective

This work grew into the [Part I nonuniformity paper](../../papers/malarich-nonuniform1/).
I gave this talk early in the project, when I was using Monte-Carlo simulations to see how well a fitting technique would work at different noise levels.
The next month I gave my prelim exam, and a professor asked whether the problem was still underdetermined if I didn't have any measurement noise.

For this presentation I added noise to each comb tooth of a simulated dual-comb spectrum.
I was fixated on the capabilities of the particular spectrometer I was using.
Eventually I would look at integrated areas of absorption features to explore the limits of absorption spectroscopy for this technique,
if you could measure as many absorption features with whichever lower-state energies you want.