---
layout: talk
title: Countering nonlinearity in digitization for precise dual-frequency comb spectroscopy
authors: N Malarich, K Cossel, F Giorgetta, E Baumann, G Mead, D Herman, B Washburn, N Newbury, I Coddington
venue: Vancouver
event: Optical Sensing Congress
eventlong: Optical Sensors and Sensing Congress
era: nist
project: accuracy
year: 2022
doi: 10.1364/ES.2022.EM3D.2
---

## Abstract

In order to measure gas concentrations with sub-percent accuracy,
we measure, simulate, and propose solutions for removing analog to digital converter (ADC) imposed bias
on the recorded interferograms in dual comb spectroscopy.


## Retrospective

This work was a side initiative during the open-path oxygen measurements I [presented at CLEO](../o2/).
The group had long known that all the hardware after the photodetector can change the apparent gas concentration by about 1%.
They'd seen that a multichannel DAQ would produce different measurements with the same signal split into different ADC channels,
but that adding RF amplifiers or mixing an electronic sine wave could bring the concentrations into agreement.
Group meeting discussions led to repeated disagreements about the precise cause of this hardware-dependence.

Ian Coddington got us to measure the AC-coupled ADC response directly with a sine-wave input,
and from the resulting Integrated Non-Linearity plots we were able to refine a model of the effect of all the hardware components on the recorded laser signal.

The project is now [published in Optics Express](../../papers/malarich-adc/).