---
layout: paper
title: Supersonic combustion diagnostics with dual comb spectroscopy
image: /images/papers/velocimetry.jpg
authors: D Yun, NA Malarich, RK Cole, SC Egbert, JJ France, J Liu, KM Rice, MA Hagenmaier, JM Donbar, N Hoghooghi, SC Coburn, GB Rieker
ref: Yun et al. (2022) Proc. Comb. Inst.
doi: https://doi.org/10.1016/j.proci.2022.07.103
arxiv: https://arxiv.org/abs/2205.04891
journal: "Proceedings of the Combustion Institute"
year: 2022
project: scramjet
era: PhD
---

## Abstract

Hypersonic engine development requires accurate and detailed measurements of fluidic and thermodynamic parameters 
to optimize engine designs and benchmark computational fluid dynamic (CFD) simulations. 
Here, we demonstrate that dual frequency comb spectroscopy (DCS) with mode-locked frequency combs can provide simultaneous absolute measurements 
of several flow parameters with low uncertainty across a range of conditions owing to the broadband and ultrastable optical frequency output of the lasers. 
We perform DCS measurements across a 6800-7200 cm^-1 bandwidth covering hundreds of H2O absorption features resolved with a spectral point spacing of 0.0067 cm^-1 
and point spacing precision of 1.68 X 10^-10 cm^-1.
We demonstrate 2D profiles of velocity, temperature, pressure, water mole fraction, and air mass flux in a ground-test dual-mode ramjet at Wright-Patterson Air Force Base. 
The narrow angles of the measurement beams offer sufficient spatial resolution to resolve properties across an oblique shock train in the isolator 
and the thermal throat of the combustor. 
We determine that the total measurement uncertainties for the various parameters range from 1% for temperature to 9% for water vapor mole fraction, 
with the absorption database/model that is used to interpret the data typically contributing the most uncertainty (leaving the door open for even lower uncertainty in the future). 
CFD at the various measurement locations show good agreement, largely falling within the DCS measurement uncertainty for most profiles and parameters. 

## Less-technical description
We took our laser system to the Air Force base in Dayton to measure air velocity through their test-rig scramjet.
Scramjets capture supersonic air, slow it down, and then inject fuel to combust with the air and produce even higher speeds out the back of the engine.
To evaluate how much thrust these scramjets generate, scientists want to know the air conditions throughout the engine.
One can either simulate the air conditions using computational-fluid-dynamics codes, or measure the gas using instruments such as dual-comb spectrometers.

We used dual-comb spectroscopy to measure the air conditions.
By splitting the laser light across two crossed paths (using the optical setup in the picture), we can see the difference in the measurements between the two paths and determine air velocity.
We took measurements at several locations in the scramjet and found good agreement with the Air Force's physics simulations.
This agreement occurred even with single-blind measurements, where we fit our laser data for gas conditions before comparing to existing simulations.

### My contribution
I worked with the rest of the team pre-campaign system tests and travelled to the measurement site.
We took measurements in 2 portions of the ramjet engine: the isolator (which slows the inlet air to subsonic speeds),
and the combustor (which generates the energies to propel the engine).
I did the data analysis for the combustor measurements.
