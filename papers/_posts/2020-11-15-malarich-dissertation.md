---
layout: paper
title: Sensing in Nonuniform-Temperature Gases Enabled by Dual Frequency Comb Spectroscopy
authors: NA Malarich
image: /images/papers/dissertation.png
ref: Malarich (2020)
journal: CU thesis
year: 2020
projects: nonuniformity, database
era: PhD
first_author: True
---

This is my PhD dissertation. It's closed-access through ProQuest, but if you search Google Scholar there should be a PDF available through CU.
Most of the chapters are also in peer-reviewed publications, but Chapters 1, 8.2, 10 have unique information.

## Abstract
Absorption spectroscopy is a useful diagnostic tool to probe extreme environments. For example, gas turbine engines on aircraft are overdesigned to ensure durable operation in the face of uncertain, extreme internal conditions. This overdesign reduces the efficiency of fossil fuel usage as well as the performance attainable by the aircraft. Laser diagnostics can improve combustion system designs by quantifying the thermodynamic conditions and kinetics of combustion in situ. Similarly, absorption spectroscopy advances our understanding of many other planets, by observing starlight interacting with their atmospheres.

Absorption spectroscopy works by quantifying the absorption of light at specific wavelengths by various molecules in the optical path. One must have detailed spectroscopic models that describe the interaction between light and gas molecules to interpret the measured signals back to thermodynamic properties of interest (temperature, molecular concentration, pressure, etc.), as well as confidence in the sensitivity of the absorption signal to the phenomenon one is measuring.

This thesis addresses two challenges for absorption spectroscopy in nonuniform-temperature gases: quantifying high-temperature spectroscopic models for planetary science and combustion, and resolving spatial temperature nonuniformity along the light path that can arise in these environments. The optical tool used for both of these projects is the dual frequency comb spectrometer, which offers high optical bandwidth and spectral resolution, as well as a precise frequency axis.

Using a broadband spectrometer such as the dual frequency comb, one can under certain circumstances infer the nature of the spatial variation of temperature along a laser path through a gas. Here, I develop a new approach of analyzing absorption spectra to determine spatial nonuniformity, and present insight into the capabilities and limitations of the approach.  I produce software for analyzing a broadband laser transmission spectrum, and prove its effectiveness in a nonuniform tube furnace in the laboratory. Finally, I demonstrate the technique on a ground-test dual-mode ramjet, and describe the challenges of implementing this technique to such high-temperature combustion environments.

Nonuniformity sensing, and more generally, accurate thermodynamic measurements using absorption spectroscopy, require spectroscopic absorption models for the molecules present in the interrogated gas. These models, catalogued in multiple databases, come from both theory and experiment. Here, I leverage the optical bandwidth and resolution of the dual frequency comb spectrometer to collect laboratory methane absorption data from 300-1000 Kelvin to update absorption models for high temperature conditions.
