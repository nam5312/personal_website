---
layout: paper
title: Using open-path dual-comb spectroscopy to monitor methane emissions from simulated grazing cattle
image: /images/papers/ksu_control_release.png
authors: C Weerasekara, LC Morris, NA Malarich, FR Giorgetta, DI Herman, KC Cossel, NR Newbury, CE Owensby, SM Welch, C Blaga, BD DePaola, I Coddington, EA Santos, BR Washburn
ref: Weerasekara et al. (2024) Atmos. Meas. Tech.
doi: https://doi.org/10.5194/amt-17-6107-2024
journal: "Atmospheric Measurement Techniques"
open: True
year: 2024
project: outdoor
era: nist
---

## Abstract

Accurate whole-farm or herd-level measurements of livestock methane emissions are necessary for anthropogenic greenhouse gas inventories and to evaluate mitigation strategies. A controlled methane (CH<sub>4</sub>) release experiment was performed to determine if dual comb spectroscopy (DCS) can detect CH<sub>4</sub> concentration enhancements produced by a typical herd of beef cattle in an extensive grazing system. Open-path DCS was used to measure downwind and upwind CH<sub>4</sub> concentrations from ten point-sources of methane simulating cattle emissions. The CH<sub>4</sub> mixing ratio along with wind velocity data were used to calculate CH<sub>4</sub> flux using an inverse dispersion model, and the simulated fluxes were then compared to the actual CH<sub>4</sub> release rate. For a source located 60 m from the downwind path, the DCS system detected 10 nmol mol<sup>-1</sup> CH<sub>4</sub> horizontal concentration gradient above the atmospheric background concentration with a precision of 6 nmol mol<sup>-1</sup> in 15-min interval. A CH<sub>4</sub> release of 3970 g day<sup>-1</sup> was performed resulting in an average concentration enhancement of 24 nmol mol<sup>-1</sup> of CH<sub>4</sub>. The calculated CH<sub>4</sub> flux was (4002±1498) g day<sup>-1</sup> in agreement with the actual release rate. Periodically altering the downwind path, which may be needed to track moving cattle, did not adversely affect the ability to determine the CH<sub>4</sub> flux. The measurement was only limited by maintaining sufficient reflected power from the remote retroreflectors over the open path to achieve a sufficient signal-to-noise ratio. These results give us confidence that CH<sub>4</sub> flux can be determined by grazing cattle with low disturbance and direct field-scale measurements.


## Less-technical description

Much methane emissions come from cow burps, and most of those emissions come from the first 3 years of the beef cattle's life when they are grazing in the fields, before they fatten up in the feedlot during their last 6 months. We believe we can use lasers to monitor the methane emissions from a small herd of 20 grazing cattle, based on previous work using the same technology to measure methane from [oil wells](https://doi.org/10.1364/OPTICA.5.000320) and [cattle feedlots](https://doi.org/10.1126/sciadv.abe9765). The extra challenges with cattle grazing are 1) the emissions are small, 2) they are more distributed in space, and 3) the cows move.

These are the first demonstration measurements, not on actual cattle, but at the same hilltop site where cattle herds seasonally graze. Rather than a herd of 20 cows, the emissions are from a compressed-gas cylinder routed to 20 teflon tubes, spaced along the ground at roughly where the 20 cow mouths would be. It's simpler to test on this methane-bottle emissions than on an actual cattle herd, where you'd need to monitor how the cows are moving throughout the day.

## My contribution

I helped with the laser build and built the backbone of the fitting code to get CH<sub>4</sub> concentrations.