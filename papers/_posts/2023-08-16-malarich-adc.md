---
layout: paper
title: Removing biases in dual frequency comb spectroscopy due to digitizer nonlinearity
image: /images/papers/adc.png
authors: NA Malarich, KC Cossel, JD Deschenes, FR Giorgetta, BR Washburn, NR Newbury, J Genest, and I Coddington
ref: Malarich et al. (2023) Optics Express
doi: https://doi.org/10.1364/OE.497497
data: https://doi.org/10.6084/m9.figshare.23841900
journal: "Optics Express"
open: True
year: 2023
project: accuracy
era: nist
first_author: True
---

## Abstract

Operation of any dual-comb spectrometer requires digitization of the interference signal before further processing. Nonlinearities in the analog-to-digital conversion can alter the apparent gas concentration by multiple percent, limiting both precision and accuracy of this technique. This work describes both the measurement of digitizer nonlinearity and the development of a model that quantitatively describes observed concentration bias over a range of conditions. We present hardware methods to suppress digitizer-induced bias of concentration retrievals below 0.1%.


## Less-technical description

We use dual-comb spectrometers to measure accurate greenhouse gas concentrations. But the instrument has lots of hardware components, and we want to make sure that none of these components reduce the accuracy of the concentration measurement. It turns out the worst offender in our instrument is the very last piece of hardware, the digitizer (everything we use after this to determine gas concentration is software). We'd known about this issue for years, and tried some things to correct it, but the corrections were more of a guess-and-check approach. For this paper, we came up with a quantitative model to predict how the digitizer changes the measurement. This model told us that the way we had been correcting for the digitizer isn't perfect, so we took some suggestions from the electrical engineering community to add electronics to best correct this digitizer inaccuracy.
