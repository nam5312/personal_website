---
layout: paper
title: Field deployable processing methods for stay-in-place transducers
image: /images/papers/transducer.PNG
authors: NA Malarich, CJ Lissenden, BR Tittmann
year: 2018
ref: Malarich et al. (2018) AIP
year: 2018
journal: AIP Conference Proceedings
doi: https://doi.org/10.1063/1.5031575
open: open-access
project: transducers
era: undergrad
---

Dr. Cliff Lissenden gave a conference talk about my undergraduate thesis project. This is the accompanying conference paper.
The image shows several commercial tape-on transducers, and my spray-on transducer in the bottom right.

## Abstract

Condition monitoring provides key data for managing the operation and maintenance of mechanical equipment in the power generation, chemical processing, and manufacturing industries. Ultrasonic transducers provide active monitoring capabilities by wall thickness measurements, elastic property determination, crack detection, and other means. In many cases the components operate in harsh environments that may include high temperature, radiation, and hazardous chemicals. Thus, it is desirable to have permanently affixed ultrasonic transducers for condition monitoring in harsh environments. Spray-on transducers provide direct coupling between the active element and the substrate, and can be applied to curved surfaces. We describe a deposition methodology for ultrasonic transducers that can be applied in the field. First, piezoceramic powders mixed into a sol-gel are air-spray deposited onto the substrate.
Powder constituents are selected based on the service environment in which the condition monitoring will be performed. Then the deposited coating is pyrolyzed and partially densified using an induction heating system with a custom work coil designed to match the substrate geometry. The next step, applying the electrodes, is more challenging than might be expected because of the porosity of the piezoelectric coating and the potential reactivity of elements in the adjacent layers. After connecting lead wires to the electrodes the transducer is poled and a protective coating can be applied prior to use.
Processing of a PZT-bismuth titanate transducer on a large steel substrate is described along with alternate methods.