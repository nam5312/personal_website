---
layout: paper
title: Apportionment and inventory optimization of agriculture and energy sector methane emissions using multi-month trace gas measurements in Northern Colorado
image: /images/papers/pao_gas.jpg
authors: GJ Mead, DI Herman, FR Giorgetta, NA Malarich, E Baumann, BR Washburn, N Newbury, I Coddington, KC Cossel
ref: Mead et al. (2024) GRL
doi: https://doi.org/10.1029/2023GL105973
journal: "Geophysical Research Letters"
open: True
year: 2024
project: outdoor
era: nist
---

## Abstract

Quantifying sector-resolved methane fluxes in complex emissions environments is challenging yet necessary to improve emissions inventories and guide policy. Here, we separate energy and agriculture sector emissions using a dynamic linear model analysis of methane, ethane, and ammonia data measured at a Northern Colorado site from November 2021 to January 2022. By combining these sector-apportioned observations with spatially resolved inventories and Bayesian inverse methods, energy and agriculture methane fluxes are optimized across the study's 850 km<sup>2</sup> sensitivity area. Energy sector fluxes are synthesized with previous literature to evaluate trends in energy sector methane emissions. Optimized agriculture fluxes in the study area were 3.5× larger than inventory estimates; we demonstrate this discrepancy is consistent with differences in the modeled versus real-world spatial distribution of agricultural sources. These results highlight how sector-apportioned methane observations can yield multi-sector inventory optimizations in complex environments.


## Less-technical description

How to measure greenhouse gas emissions when there are so many small sources?

If you're using dual-comb instruments, the main approach has been to launch the laser on two different outdoor paths. When the wind is blowing in the right direction, then one path is upwind and the other path is downwind of the emissions source. Using a wind guage and the concentration difference between the two laser paths, you can quantify the emissions in between the laser paths. There might be all sorts of emissions upwind of both lasers, but that upwind laser path can isolate the local emissions from these further away regional emissions.

This work used a different approach, following the style of many air-sampling point-measurement stations. It only used one laser path, but measured multiple greenhouse gases. The laser path was on a dilapidated old government property (sold a year after the measurements ended) with no emissions within the radius of the laser path, but the surrounding county has plenty of methane emissions from agriculture as well as oil and gas operations. Whenever the wind blows from a large animal farm, it blows ammonia with it; when the wind blows from a leaky oil and gas site, it blows ethane along with it. By looking at the time-varying concentrations of methane, ethane, and ammonia as many plumes blow across the laser path, we determined how much excess methane concentration was due to agriculture vs oil and gas.

Now how to turn that methane concentration into an emissions estimate? It takes a map of expected sources and some model of how the methane plumes disperse from the sources as the wind blows. In this case, the map of sources came from government inventories of livestock-heavy farms and oil-and-gas fracking rates. The wind model was a very involved 48-hour weather model over a very large region (implemented with the [STILT-R](https://uataq.github.io/stilt/#/) software package). The weather model tells what fraction of the air crossing the laser beam in a given hour came from each grid cell of the larger region of Northern Colorado, given the last two days of weather. It turns out that over the span of months of measurement, the winds came pretty uniformly from every direction in a 10 mile radius. Any farms or oil infrastructure further away had the plumes dilute too much to see much of a methane enhancement.

## My contribution

I helped build and deploy the dual-comb spectrometer. I also helped respond to peer review comments.