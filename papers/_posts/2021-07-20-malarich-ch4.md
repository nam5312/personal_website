---
layout: paper
title: Dual frequency comb absorption spectroscopy of CH<sub>4</sub> up to 1000 Kelvin from 6770-7630 cm<sup>-1</sup>
image: /images/papers/ch4.PNG
authors: NA Malarich, D Yun, K Sung, S Egbert, SC Coburn, BR Drouin, GB Rieker
ref: Malarich et al. (2021) JQSRT HIT20 spec. iss.
doi: https://doi.org/10.1016/j.jqsrt.2021.107812
journal: "JQSRT"
year: 2021
project: database
era: PhD
first_author: True
---

## Abstract

As infrared telescopes are finding evidence of methane in hot exoplanet atmospheres, 
it is becoming increasingly important to have accurate high-temperature methane absorption models.
To evaluate and update several spectroscopic databases (HITRAN2016, HITEMP, ExoMol), we collected laboratory spectra of the methane icosad
(6770-7570 cm<sup>-1</sup>) using a 200 MHz (0.0067 cm<sup>-1</sup>) dual frequency comb spectrometer.
The pure methane data span 18 to 300 Torr and 296 to 1000 K.
We found good agreement with the HITRAN2016 model spectrum at room temperature,
and substantial mismatch between our spectra and all absorption models at elevated temperature.
We present several updates to HITRAN2016 to improve agreement with the measured high-temperature spectra.
Specifically, we assign 4283 lower-state energies which had previously been given a default value in HITRAN2016 (E"= 999 cm<sup>-1</sup>),
update existing lower-state energies for 92 features, and add 293 new high-temperature features in order to improve HITRAN2016 above 300 K.
Additionally, we update the band-wide line positions by 0.001 cm<sup>-1</sup>, the self-widths by +7%,
and we estimate band-averaged temperature-dependence exponents for self-width (0.85) and self-shift (0.58).
These measurements are an important step towards merging empirical and theoretical high-temperature methane databases, 
with the goal of enabling better understanding of exoplanet atmospheres.

## Less-technical description

In order to use light (from lasers or telescopes) to identify the chemical makeup of gases,
one needs an accurate database of how different molecules absorb light at each frequency.
One way to produce the database is to calculate the absorption signature from quantum mechanics.
It is difficult to do this for molecules like methane, particularly at higher temperatures.

We took laboratory furnace measurements of methane at known, controlled temperatures and pressures,
and used those measurements to calibrate and expand these absorption databases.
We did this work with staff scientists at the Jet Propulsion Laboratory,
who have developed the software to improve absorption models with laboratory data.

The picture shows one way how the size of all the different features in the methane spectrum change with temperature.
Points on the right of the plot indicate features that get larger at higher temperature,
and are hard to see in room-temperature measurements.

This improved hot methane database will be particularly useful for telescope measurements of newly-discovered planets
orbiting other suns. Methane in the atmosphere could be a sign of life on these planets.

