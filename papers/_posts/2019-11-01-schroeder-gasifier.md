---
layout: paper
title: Temperature and concentration measurements in a high-pressure gasifier enabled by cepstral analysis of dual-frequency comb spectroscopy
image: /images/papers/gasifier_square.jpg
authors: PJ Schroeder, AS Makowiecki, MA Kelley, RK Cole, NA Malarich, RJ Wright, JM Porter, GB Rieker
doi: https://doi.org/10.1016/j.proci.2020.06.011
year: 2020
ref: Schroeder et al. (2020) Proc. Comb. Inst.
journal: "Proceedings of the Combustion Institute"
era: PhD
---

## Abstract

High-pressure gasification processes are important for conversion of solid materials into gaseous fuels and other chemicals.
Laser absorption diagnostics are an important means to study these processes, but are challenging to implement due to the extreme temperatures and pressures present in the system.
Here, we combine broadband high-resolution dual-comb spectroscopy with an advanced spectral absorption database and a new means for baseline-free absorption spectroscopy analysis
to enable measurements of temperature and water vapor concentration in the core of an entrained flow gasifier operating at up to 1700 K and 15 bar.
The dual-comb spectrometer measures the absorption of water vapor from 6800 to 7150 cm<sup>−1</sup> with a point spacing of 0.0067 cm<sup>−1</sup>.
The bandwidth is helpful for resolving the complex, congested absorption fingerprint of water vapor that is used to determine the species concentrations and temperature.
We interpret the spectrum with absorption models based on a database measured under carefully controlled high-temperature conditions with the dual-comb spectrometer.
The database includes the pressure broadening, shift, and temperature dependence of these parameters for water vapor in argon, which is the gasifier bath gas.
Finally, fitting the absorption model to the data is enabled by modified free induction decay analysis, which is an approach for quantitatively obtaining species and temperature information without determining the baseline intensity of the spectrometer. 
The baseline-free approach is crucial to success in this environment, where there are no non-absorbing regions of the spectrum to anchor the normalization of the laser intensity as in traditional direct absorption spectroscopy.
We demonstrate good agreement with temperatures measured on the reactor core via optical pyrometry, and show that water vapor concentrations in the reactor core did not reach the expected system set points during some experiments.

## Less-technical description

My labmate, Paul, took the dual frequency comb laser down to Colorado School of Mines to make temperature and concentration measurements in a test-rig coal gasifier.
Vaporizing pieces of coal into desired gaseous fuels is complicated, so laser measurements of temperature, pressure, and composition can help engineers to optimize the process conditions.
It turns out that interpreting laser measurements in coal gasifier conditions is also complicated, so several intermediate publications happened between Paul's first gasifier measurements and this publication, and people like me who didn't take part in the experiments ended up on the paper.

We determine temperature and concentration from the strength of the water vapor absorption signal. The trouble is we don't directly measure the water vapor absorption signal--we measure the transmitted laser light and infer how much laser light went into the system to determine how much light then got absorbed.
When you take laser measurements in human-survivable conditions, it's easy to determine the original laser light spectrum from all the parts in the transmission spectrum that have no water vapor absorption signal.
The trouble is in the gasifier, the water vapor is at such high pressure that it absorbs all laser light frequencies.
Amanda Makowiecki and Ryan Cole figured out how to get around this problem with "cepstral analysis."

### My contribution
Before Amanda figured out the cepstral analysis technique, I took a crack at the data fitting, by simultaneously fitting the entire spectrum with about 100 extra fitting parameters of a Chebyshev polynomial. This was more robust than the existing technique, but it took forever.
Amanda pointed out that the Chebyshev technique took about 30 minutes, whereas the cepstral technique took about 2 minutes.
I then formatted the water vapor absorption database to work with Amanda's technique for the fits in the final paper.

