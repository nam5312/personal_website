---
layout: paper
title: Pulse interaction induced systematic errors in dual comb spectroscopy
image: /images/papers/xpm.png
authors: M Walsh, E Baumann, N Malarich, S Egbert, RK Cole, GB Rieker, NR Newbury, I Coddington, K Cossel, J Genest
ref: Walsh et al. (2024) Opt. Exp.
doi: https://doi.org/10.1364/OE.523623
journal: "Optics Express"
open: True
year: 2024
project: accuracy
era: nist
---

## Abstract

Systematic errors are observed in dual comb spectroscopy when pulses from the two sources travel in a common fiber before interrogating the sample of interest. When sounding a molecular gas, these errors distort both the line shapes and retrieved concentrations.
Simulations of dual comb interferograms based on a generalized nonlinear Schrodinger equation highlight two processes for these systematic errors. Self-phase modulation changes the spectral
content of the field interrogating the molecular response but affects the recorded spectral baseline
and absorption features differently, leading to line intensity errors. Cross-phase modulation
modifies the relative inter-pulse delay, thus introducing interferogram sampling errors and
creating a characteristic asymmetric distortion on spectral lines. Simulations capture the shape
and amplitude of experimental errors which are around 0.1% on spectral transmittance residuals
for 10 mW of total average power in 10 meters of common fiber, scaling up to above 0.6% for 20 mW and 60 m.


## Less-technical description

We use the dual-comb lasers to measure accurate outdoor gas concentrations in many places. But many times when we deploy the laser, we can't put it very close to the telescope that actually launches the light outside. That fiber that connects the laser with the telescope can distort the measurement through some nonlinear optics effects described in this paper. This distortion gets worse when the laser is higher power or when the fiber distance to the telescope is longer.

## My contribution

This work kept following me around, through grad school and my postdoc. I was one of the people who kept finding these characteristic residual structures in the spectral fits and initiating or being in the table during the discussions that remained hand-wavey until Jerome came to NIST on sabbatical and set up the Schrodinger equation simulations. I collected one of the example spectra that appears in the paper introduction, was insistent that the pre-chirp fiber did not remove the XPM, and served as a person without formal nonlinear optics education for Jerome to talk to as he developed the explanations in the paper.