---
layout: paper
title: Validation of open-path dual-comb spectroscopy against an O<sub>2</sub> background
image: /images/papers/o2.jpeg
authors: NA Malarich, BR Washburn, KC Cossel, GJ Mead, FR Giorgetta, DI Herman, N Newbury, I Coddington
ref: Malarich et al. (2023) Optics Express
doi: https://doi.org/10.1364/OE.480301
data: https://doi.org/10.6084/m9.figshare.21506976
journal: "Optics Express"
open: True
year: 2023
project: accuracy
era: nist
first_author: True
---

[Erratum to this manuscript](https://doi.org/10.1364/OE.523295)

## Abstract

Dual-comb spectroscopy measures greenhouse gas concentrations over kilometers of open air with high precision. However, the accuracy of these outdoor spectra is challenging to disentangle from the absorption model and the fluctuating, heterogenous concentrations over these paths. Relative to greenhouse gases, O<sub>2</sub> concentrations are well-known and evenly mixed throughout the atmosphere. Assuming a constant O<sub>2</sub> background, we can use O<sub>2</sub> concentration measurements to evaluate the consistency of open-path dual-comb spectroscopy with laboratory-derived absorption models. To this end, we construct a dual-comb spectrometer spanning 1240 nm to 1700 nm, which measures O<sub>2</sub> absorption features in addition to CO<sub>2</sub> and CH<sub>4</sub>. O<sub>2</sub> concentration measurements across a 560 m round-trip outdoor path reach 0.1% precision in 10 minutes. Over seven days of shifting meteorology and spectrometer conditions, the measured O<sub>2</sub> has -0.07% mean bias, and 90% of the measurements are within 0.4% of the expected hemisphere-average concentration. The excursions of up to 0.4% seem to track outdoor temperature and humidity, suggesting that accuracy may be limited by the O<sub>2</sub> absorption model or by water interference. This simultaneous O<sub>2</sub>, CO<sub>2</sub>, and CH<sub>4</sub> spectrometer will be useful for measuring accurate CO<sub>2</sub> mole fractions over vertical or many-kilometer open-air paths, where the air density varies.


## Less-technical description

This is an upgrade to the dual-comb instrument that measures outdoor carbon-dioxide and methane concentrations.
We expand the laser to different wavelengths so it can measure a different molecule (oxygen), and also measure it accurately.
The image shows an outdoor measurement from the dual-comb instrument, showing light in two wavelength regions.
The 1270 nm light is a new development for this work, which contains absorption features from oxygen and water vapor.
There is also DCS signal at 1600 nm, the traditional wavelength region to measure carbon dioxide and methane and water-vapor.

Oxygen is not as interesting a molecule as carbon dioxide--it's the same concentration all around the world.
You can't use it to see how much cities and industries are polluting.
But because we know the exact oxygen concentration,
we used it to test the accuracy of the dual-comb concentration measurement.
Eventually, we intend to use this oxygen measurement to recover carbon dioxide concentrations more accurately.



