---
layout: paper
title: Resolving non-uniform temperature distributions with single-beam absorption spectroscopy II) Implementation from broadband spectra
image: /images/papers/nonuniform_nc.PNG
authors: NA Malarich, GB Rieker
year: 2021
ref: Malarich, Rieker (2021) JQSRT
journal: "JQSRT"
doi: https://doi.org/10.1016/j.jqsrt.2021.107805
arxiv: https://arxiv.org/abs/2011.06638
data: https://doi.org/10.5281/zenodo.4268359
code: https://gitlab.com/nam5312/ntfit
project: nonuniformity
era: PhD
first_author: True
---

## Abstract

Several past studies have described how absorption spectroscopy can be used to determine spatial temperature variations along the optical path by measuring the unique, nonlinear response to temperature of many molecular absorption transitions and performing an inversion.
New laser absorption spectroscopy techniques are well-suited to this nonuniformity measurement, yet present analysis approaches use only isolated features rather than a full broadband spectral measurement.
In this work, we develop a constrained spectral fitting technique called E"-binning to fit an absorption spectrum arising from a nonuniform environment.
The information extracted from E"-binning is then input to the inversion approach from the [previous paper in this series](../malarich-nonuniform1/) to determine the temperature distribution.
We demonstrate this approach by using dual frequency comb laser measurements to resolve convection cells in a tube furnace. The recovered temperature distributions at each measurement height agree with an existing natural convection model.
Finally, we show that for real-world measurements with noise and absorption model error, increasing the bandwidth and the number of measured absorption transitions may improve the temperature distribution precision.
We make the fitting code publicly available for use with any broadband absorption measurement.

## Non-technical description

We predicted that the new, predominant laser in the Rieker lab would be well-suited to measure temperature nonuniformity in combustion systems.
In this paper we demonstrate this measurement on a non-combusting laboratory system.
There are two exciting parts of this paper to me. First, the visual from the companion paper inspired a [data-fitting algorithm](https://gitlab.com/nam5312/ntfit)
which takes advantage of the data from some newer lasers, and it also builds on my labmates' faster fitting codes.
Basically, the theory from [our previous paper](../malarich-nonuniform1/) showed that a laser measurement containing 100,000 frequencies of light
does not contain anywhere near 100,000 points of unique information about gas temperature.
Therefore, we can group together much of this redundant data to produce a more robust fit of the temperature nonuniformity.

Second, the laser measurements uncovered some unanticipated physics (natural convection) in this laboratory system,
which were not obvious with the limited thermocouple measurements alone.
These complex forms of heat transfer are one reason why we prefer lasers instead of simpler thermocouples to make these temperature measurements in combustion.

The image shows two temperature distributions measured at two heights in the laboratory system,
and the agreement with a third-party physics model.
Our DCS measurement produced a curve on the temperature image where a traditional laser measurement would only produce a horizontal line.
This temperature nonuniformity measurement allows us to better constrain the boundary conditions of this heat transfer model.