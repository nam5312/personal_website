---
layout: paper
title: Finding and minimizing systematic errors in dual comb interferometry
image: /images/papers/bias.png
authors: M Walsh, P Guay, JD Deschenes, N Malarich, I Coddington, K Cossel, J Genest
ref: Walsh et al. (2025) J Phys. B 
journal: "Journal of Physics B: Atomic, Molecular, and Optical Physics"
doi: https://doi.org/10.1088/1361-6455/adac94
year: 2025
project: accuracy
era: nist
---

## Abstract

With sufficiently high signal-to-noise, several systematic errors become prominent in dual-comb interferometry measurements. This paper reviews several error sources including electrical, photo-detection, amplification and acquisition chain non-linearity. Sources of optical non-linearity such as self-phase modulation, cross-phase modulation and Raman soliton shifting are also covered, as are spectral fringing due to parasitic reflections and back-scattering. The non-linear response of the target sample itself can also be a source of errors. Methods to identify and minimize errors in experimental data are discussed. Good practices, instrument design strategies and tools, such as the dynamic range diagram, are suggested.


## Less-technical description

This is a summary paper of all of Jerome's accuracy work at Laval and NIST. Many of the sections owe to Jerome's sabbatical at NIST, in which there was much discussion even past the first 6 months. It provides extra detail on top of the Laval/NIST papers it cites to help you fully appreciate Jerome's opinions and implement them.

## My contribution

I provided the measurements for the digitizer portion of the paper, although this time Jerome got to emphasize different recommendations for solving it.