---
layout: paper
title: Open-path measurement of stable water isotopologues using mid-infrared dual-comb spectroscopy
image: /images/papers/pao_van.jpg
authors: DI Herman, G Mead, FR Giorgetta, E Baumann, N Malarich, BR Washburn, NR Newbury, I Coddington, and KC Cossel
ref: Herman et al. (2023) Atmos. Meas. Tech.
doi: https://doi.org/10.5194/amt-16-4053-2023
journal: "Atmospheric Measurement Techniques"
open: True
year: 2023
project: outdoor
era: nist
---

## Abstract

We present an open-path mid-infrared dual-comb spectrometer (DCS) capable of precise measurement of the stable water isotopologues H<sub>2</sub><sup>16</sup>O and HD<sup>16</sup>O. This system ran in a remote configuration at a rural test site for 3.75 months with 60% uptime and achieved a precision of <2‰ on the normalized ratio of H<sub>2</sub><sup>16</sup>O and HD<sup>16</sup>O (δD) in 1000 seconds. Here, we compare the δD values from the DCS to those from the National Ecological Observatory Network (NEON) isotopologue point sensor network. Over the multi-month campaign, the mean difference between the DCS δD values and the NEON δD values from a similar ecosystem is <2‰ with a standard deviation of 18‰, which demonstrates the inherent accuracy of DCS measurements over a variety of atmospheric conditions. We observe time-varying diurnal profiles and seasonal trends that are mostly correlated between the sites on daily time scales. This observation motivates the development of denser ecological monitoring networks aimed at understanding regional and synoptic scale water transport. Precise and accurate open-path measurements using DCS provide new capabilities for such networks.


## Less-technical description

This is part of a paper series on what you can measure if you put a mid-infrared laser in the field. By pushing the laser to lower frequencies, from the near-infrared to the mid-infrared, you can measure more gas molecules with higher precision. One of the molecules with a strong signal in this data was a heavy water isotopologue HDO, where one of the two hydrogen atoms in the water molecule has an extra neutron. The signal was so strong that we could measure the ratio of normal water to heavy water very precisely (sub-percent).

Why measure this ratio? It tells us how the water evaporated. Whether the water vapor came from the cold northern ocean, the warm southern gulf, or the nearby semi-arid farms.

Here's a [press release](https://www.optica.org/en-us/about/newsroom/news_releases/2022/june/researchers_measure_atmospheric_water_vapor_using/) for Dan's conference talk on the subject.

### My contribution

I helped with the laser build and deployment, and with the temperature correction to the δD.