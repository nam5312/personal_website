---
layout: paper
title: Spray-on comb transducers for health monitoring of high-temperature structures
authors: NA Malarich
image: /images/papers/transducer.PNG
doi: https://honors.libraries.psu.edu/catalog/25219
open: open-access
ref: Malarich (2015)
year: 2015
journal: PSU thesis
project: transducers
era: undergrad
first_author: True
---

This is my undergraduate thesis, available through the sidebar link from Penn State libraries.

## Abstract

Currently there is great industrial interest in installing permanent diagnostic sensors for structural health monitoring (SHM) of safety-critical parts. The past half-century has seen broad use of ultrasonic transducers for subsurface viewing and defect detection. However, traditional techniques require a water-based couplant between the transducer and interrogated structure. Thus most current sensors cannot interrogate structures near or above 100°C, such as heat-exchanger pipes and valves in an operating nuclear reactor. A network of permanently-attached guided-wave sensors on these structures could assess the structures’ state of damage and wear in order to prevent catastrophic failure. No sensor exists to monitor the health of a large volume of safety-critical structure for which the specific failure site is unpredictable. This thesis details two steps of progress towards fulfilling that need with a novel leave-in-place nonlinear ultrasonic transducer. The work combines the recent research on guided wave health inspection methods with the research on high-temperature spray-on transducers. First, the spray-on transducer is shown to be capable of fabrication and acoustic wave generation on two different structures: small-diameter heat-exchanger tubing, and a large steel mockup structure. Second, two spray-on transducers propagated along 0.2m of tubing the L(0,4) guided wave mode, a useful guided wave mode for the highly defect-sensitive method of nonlinear testing. 