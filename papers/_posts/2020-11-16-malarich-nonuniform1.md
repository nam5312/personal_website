---
layout: paper
title: Resolving non-uniform temperature distributions with single-beam absorption spectroscopy I) Theoretical capabilities and limitations
image: /images/papers/nonuniform_s.png
authors: NA Malarich, GB Rieker
year: 2020
ref: Malarich, Rieker (2020) JQSRT
journal: "JQSRT"
doi: https://doi.org/10.1016/j.jqsrt.2020.107455
data: https://doi.org/10.5281/zenodo.3930910
arxiv: https://arxiv.org/abs/2008.01522
project: nonuniformity
era: PhD
first_author: True
---

## Abstract

Absorption spectroscopy is traditionally used to determine average gas temperature and species concentration along the laser line-of-sight 
by measuring the magnitude of two or more absorption transitions with different temperature dependence. 
Previous work has shown that the nonlinear temperature dependence of the absorption strength of each transition, 
set by the lower-state energy, E”, can be used to infer temperature variations along the laser line-of-sight. 
In principle, measuring more absorption transitions with broader bandwidth light sources 
improves the ability to resolve temperature variations.  
Here, we introduce a singular value decomposition framework to this problem in order to explore 
the theoretical limits to the single-beam line-of-sight measurement approach. 
We show that in the absence of measurement noise or error, only the first ~13 well-selected absorption features improve the temperature resolution. 
A Tikhonov regularization method improves the accuracy of the temperature inversion, 
particularly for recovery of the maximum gas temperature along the laser beam. 
We use mathematical arguments as well as inversion simulations to demonstrate that for the systems we studied, at best, one can resolve a selection of temperature distributions along a laser beam line-of-sight to within 30K at best  . In part II of this work, we explore the influence of measurement noise and error and experimentally demonstrate the technique to show that under real conditions there is benefit to measuring additional absorption transitions.

## Less-technical description

This paper establishes the mathematical theory of a particular laser measurement.

For decades scientists have used lasers to measure gas temperature.
The laser shines through a gas, and then the transmitted laser signal informs the average gas temperature across the laser path.
In the traditional measurement, even if the gas contains hot and cold regions, the laser measurement provides a single temperature.

We predicted that the new dual-comb laser could recover more information about temperature nonuniformity in combustion systems than more traditional lasers used for thermometry.
Because the dual-comb produces light at so many frequencies, we might leverage that extra frequency information to recover more spatial temperature information.
In this work we dug deeper into one aspect of the mathematics than others have with this problem.
We related this physics problem to the graduate-level mathematics of inverse problems, 
made some cool visual explanations of the mathematics, and showed how two of [textbook heuristics](https://www.sciencedirect.com/book/9780123850485/parameter-estimation-and-inverse-problems)
extend to our laser measurement.

