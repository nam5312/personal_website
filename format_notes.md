## Web design guide

I modelled this website after the [Neimeyer](https://niemeyer-research-group.github.io/) and (Bedford)[bedford.io] websites.
I didn't know what I was doing, and basically figured it out by copying Bedford's code
and running through the [Jekyll Tutorial](https://jekyllrb.com/docs/step-by-step/01-setup/)
and then doing trial-and-error, because the two examples don't entirely match.
I now think I know what all the files do, so here's my guide.


This is a static website,
meaning that some computer/server uses a programming language to produce an HTML file for each webpage in the website
from the files here, and then sends that information to a server that hosts the final files.
The programming language this uses is Ruby,
which has lots of downloadable code packages called "gems",
one of these "gems" is [Jekyll](https://jekyllrb.com/), which is the main HTML-creation package.
We'll walk through the files in this repository.

### index.html
This is the homepage of the website.
There are also directories for "papers", "projects", "blog", and "talks"
and each of these directories has an `index.html` file as well.
These are all subpages of the website.
They also have a `_posts` directory, which has a bunch of `.md` or Markdown files,
these are all subpages of each of those subpages. That's the main structure of the website,
which you can follow by the backslashes on the URL.

Each of these posts has some front-matter, a list of variables for each post/page,
and then the main text that displays on the webpage.

### _layouts
This directory has HTML files to format all of the webpages.
The `papers` webpages have a different layout from the `blog` webpage, so they have their own file in the layout.
Each of those `.md` files has a line `layout: post` or similar, indicating which file in the `_layouts` directory to use to format the webpage.

### css
If the MD files need a special HTML file to format them (in the layouts directory),
then all of the HTML files need some other code to format as a nice webpage.
If you open the HTML file from this repository on a web browser, 
it doesn't look the same as it does on the actual hosted website.
That extra formatting comes from the files in the CSS directory.
This uses the SASS "gem" (in the parlance of the programming language Ruby)
to interpret all of the files in the CSS directory.

Most of these files are taken straight from `https://github.com/blab/blotter`
which in turn were probably downloaded from particular versions of open-source CSS codes.
For instance the `bootstrap` directory is Bootstrap v4.6.0, which has some particular quirks.
There's a `style.scss` file which is the main CSS file that just imports all the functions and variables from the other files in the directory.
The only file you/I should touch is the `custom.scss` file,
which changes some variables and adds some new variables/functions,
all modifications of Bootstrap 4.6.0.
For instance, you can change the font size or webpage background color or hyperlink color by adding/changing lines to this file.

### js
This is the Javascript code, also pulled directly from [blotter](https://github.com/blab/blotter) which pulled it from open-source.
It does the cute animation-like stuff in the website.

### Gemfile

To make the website, you need to run a couple lines of Ruby code in a terminal.
I test this code on the terminal provided by "Ubuntu for Windows".
The first step is to navigate to the root directory of this repository on the terminal.
The second step is to make sure this directory has the right Ruby code on it,
all the necessary packages in all the compatible versions.

Remember, the code packages in Ruby are called "gems".
The terminal commands to install the right gems are:
```
gem install bundler
bundle install
```
This first command gets the `bundler` package/gem,
which is used to install all the other packages.
`bundle` looks to the `Gemfile` to find out which gems to install,
if necessary any relevant versions,
and then takes care of all the dependencies.
The other thing `bundle install` does is make a `Gemfile.lock`,
which is a record of all the versions of all the gems it installed (which are only associated with this directory).
https://www.rubyinrails.com/2013/12/10/what-is-gemfile-lock/

### _config.yml
After installing all the Ruby code packages, you run `bundle exec jekyll build`.
This command looks at the `_config.yml` file,
and then makes the `_site` directory, which has turned the `_posts` directories from MD files to HTML files,
and changed the `style.scss` file into a `style.css` file.
(that's how it knows to set the baseurl,
use kramdown to interpret markdown files,
and use SASS to produce the `style.css` file.)
This `_site` directory has all the information a server needs to host the site.
If you run this command on a terminal in your PC (actually `bundle exec jekyll serve`, which does both `build` and `serve`)
then you should find the website under `localhost:4000/personal_website/`.

### .gitlab-ci.yml

If I want to install this website on Gitlab's server (rather than my local PC server),
I need that `.gitlab-ci.yml` file .
This is a "continuous integration" or CI file, which tells a remote server which commands to run to produce the website from the source files.
If I was using Github, I would name this a `.travis.yml` file instead.

This CI file replicates all the setup I would otherwise do on the Ubuntu command line (but with different formatting)
first point to which version of ruby to use
then which version of version of bundler to install with ruby
then run `bundle install` to get the right version of Jekyll, everything else
then build the site with `bundle exec jekyll build -d public/`
and finally I've set this to only run on the master branch.
This must be why I can never propagate a gitlab.com website when I'm testing on other branches to get the formatting right.
Probably can change that `master` to `format` when I'm debugging things in the `format` branch

And that's it!
-------------

24 September 2022: Upgrade to a new version of formatting code, from Bootstrap v3 and Jekyll v3.

### Initial goals of upgrade

Want to run latest bedford lab blotter web design,
really just so I can separate papers by career stage
Also want to change my formatting a bit
--font size a little larger,
--different hyperlink color
--greater contrast between text and background color (shades of brown)

Bedford lab upgraded Jekyll code on April 2-3 2021
https://github.com/blab/blotter/commits/master?after=466fd2cef870854afed4e444f2a34b3d3e467402+209&branch=master&qualified_name=refs%2Fheads%2Fmaster
Here are the commit messages for the 4 relevant commits

pre-CSS: cbe98ed (this runs on localhost)
"refactor CSS compilation from LESS to SASS"
213b72e (this commit gives Javascript runtime error)
LESS is (I believe) the friendly and more usable format, 
but newer versions of Jekyll and Bootstrap prefer SASS. 
Jekyll now has built in support for SASS, 
while LESS compilation still requires therubyracer gem which provides Javascript support. 
Installing therubyracer on Apple silicon has proved difficult. 
The aim here is a simple refactor without a difference in site appearance. 
There are still a couple things left to clean up however.

"update styles for SASS + Bootstrap v4"
A couple changes of note:
1. navbar component had a major refactor going from Bootstrap v3 to v4 and there was a lot of customization required to keep working as before
2. col-md- in Bootstrap v3 is the equivalent of col-lg- in Bootstrap v4

"upgrade Jekyll to v4.2.0"
The big change in going from v3 to v4 was loss of redcarpet Markdown processing support. 
The main issue (which isn't that major) in switching to kramdown is loss of superscript via ^. 
Kramdown requires html tags of <sup> and </sup> instead.
491654d

"upgrade Bootstrap to v4.6.0"
82e0859


### Notes during upgrade

javascript runtime error under "autodetect" when bundle exec jekyll serve
fix by adding to Gemfile `gem "therubyracer"`
https://stackoverflow.com/questions/41329995/could-not-find-a-javascript-runtime-how-do-i-install-one
(see the blotter note "LESS compilation requires therubyracer. Installing therubyracer on Apple silicon has proved difficult."

when my website runs on bundle exec jekyll serve,
I can see it on http://localhost:4000/personal_website/
(because I've set my baseurl = 'personal_website'
or 127.0.0.1:4000/personal_website/



BUG NOTICE:
I've got an error running `bundle install` if I set the Gemfile to say  `gem "bundler", "2.1.4"`
even if I've got this one installed, because the default is 2.1.2
I can fix this by running `bundle _2.1.4_ install`
or by changing the Gemfile to say `gem "bundler", "~>2.1.2"` instead of requiring a particular version of bundler
https://stackoverflow.com/questions/61898186/how-to-bundle-install-gemfile-with-specific-version-of-bundler


To implement the SASS,
I needed to pull over the CSS and JS directories,
and change the _config.yml file to add that SASS version (and remove the redcarpet references, because now using Kramdown to interpret markdown (MD) files).
To change the font and color, I changed the css/custom.scss file,
to change the $body-bg color to a brownish-hex color
and to change the font from sans-serif to serif,
I added the lines
```
$font-family-serif: Georgia, "Times New Roman", Times, serif;
$font-family-base: $font-family-serif;
```
somehow changing the `body { font-family: Times New Roman;}` didn't do anything, very frustrating.
Because I updated the bootstrap version, there were different names for all the HTML boxes,
so I needed to change the layouts/default file and the main index.html file
to change the names of the bracketed boxes, match with bedford.io style.