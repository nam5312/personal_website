---
layout: post
title: Publishing in Optics Express
tag: technique
---

Publishing in Optics Express (or other Optica Publishing Group journals)

There is a logical order to formatting. If the paper is going into a peer-review process and has not yet been accepted, you don't need to perfect the manuscript format. I used MS Word with the journal's Word template, and used Zotero for citation management. I added tables manually using Word's Table method, which is required for the journal but does not
For these submissions, the figure quality does not have to be perfect, just good enough for the reviewers to read. 

I didn't bother formatting a preprint, as Optics Express is an open-access journal. Publication fee is ~$2k, compared to JQSRT which is free to publish behind their paywall and $3k for open-access (although you can update the ArXiv preprint of a paywalled article with the non-formatted accepted version).

After the paper has been accepted, you have to resubmit yet another version to the typesetter. This is the "copy revision" version. In this version you still don't have to get your figure resolution perfect, or try super hard to get all of your figures exactly on the bottom of one page rather than the top of the next, because the typesetter will subtly change these things (especially the size of the tables).

This "accepted version" submission is the time to check your author list + attributions, acknowledgements, figure + table + citation order. It's also the time to check how Zotero formatted the bibliography. This is the point to make sure the dissertations and conference papers are cited as you want, and to update the many-author citations with "et als" eg "I.E. Gordon, L.S. Rothman, R.J. Hargreaves et al. "The HITRAN2020 molecular spectroscopic database,"... It's easier to do it right before this submission, after "refreshing" Zotero for the last time, rather than figuring out how to get Zotero to do "et al" manually.

The final step is receiving the proofs from the typesetter. This is the point where you can finally fix those figures. When you click "request revisions," you can submit a commented-up PDF of the proof, and also PDF versions of your figures. The typesetter will update the proof with the PDF images, you should be able to see on Prism before the paper gets published. The "insert files" drop-down menu has an option for "marked-up proof" and "figure", but it only accepts PDF format, no PNG or EPS or anything else.

Figure placement is still a bother--the proofs tend to end up with slightly different words per line and margins than the Word template. One of the changes is the title is Arial 16 pt, and there's a 16 pt paragraph spacing before AND after the author list (and before the author affiliation list). Each new paragraph in subsection has 1/8" indent, including after figures. Figures and figure captions have extra 1/2" spacing on left and right relative to main text. Figure captions have 6pt spacing above and below paragraph, and figures have 6pt spacing above. All these things subtly change the figure placement, so it's a bit of a wild card where they end up in the proofs. I've had success requesting figure or text page shifting in the proof revisions, though.

### Formatting figures
I make plots in Python/Matplotlib and schematics in Inkscape or directly in matplotlib with the plot. These can be saved as PNG (for the MS Word submissions) or as PDFs (for the proof revision). Inkscape has a menu to select the DPI for PNG files--200 or 300 is good. To customize the DPI of Python plots, matplotlib figures should be saved in the command line, not the GUI. 
>> plt.savefig('fig5.png', dpi=300); 
>> plt.savefig('fig5.pdf')

The other change to preserve DPI in MS word is to change the MS Word settings, under Word File / Options / Advanced there is an "Image Size and Quality" section with a "Default resolution" drop-down table which I set to 220 ppi.

Unfortunately, you cannot use Word or Latex to combine multiple image files under a single figure caption. For multi-part plots, I use plt.subplots(), or to have one plot take up 2/3 of the figure, I'd use f6 = plt.figure(); gs = f6.add_gridspec(1,3); a6a = f6.add_suplot(gs[:2]); then I can further customize the figure placement with a6a.set_position([.75, .28, .24, .71])

What if I want to combine a matplotlib plot and Inkscape schematic in one PDF file? I don't know how to do that.

### Sharing data
NIST has a policy of publicly sharing the data files associated with each figure. Optica journals make this easy, by automatically placing any supplemental files in Figshare. When I first submitted to peer-review, I prepared a zip file of CSVs to submit as "Dataset 1" to the supplement drop-down menu. I was allowed to edit this zip file for each subsequent submission, up through the "copy revision" version.

I labelled these CSV files f1_description.csv, where "f1" stands for "figure 1 data." If I had 2 traces, then the CSV file had 3 columns: 1 for the x-values, 1 for the y-values of each trace in the figure. The first row of the CSV file indicated whether it was x-axis data or which entry in the legend it corresponded with.