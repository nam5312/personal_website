---
layout: paper
title: Resolving nonuniform temperature distributions to 2000 Kelvin in a scramjet combustor.
authors: NA Malarich, RK Cole, D Yun, SC Coburn, N Hoghooghi, JJ France, KM Rice, JM Donbar, GB Rieker
journal: in prep
project: scramjet
---

# Non-technical description

We took our laser system to the Air Force base in Dayton to measure air velocity through their test-rig scramjet.
We took measurements across the combustor, which had high temperatures, combustion products, and path nonuniformity.
These were new challenges for my temperature nonuniformity algorithm, which I had not yet tested in a combustion environment. I was able to compare my results against the computational fluid dynamics model from the Air Force scientists.