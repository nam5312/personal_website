---
layout: project
title: nonuniformity
longtitle: Absorption spectroscopy for gas temperature nonuniformity
era: PhD
year: 2015-2020
---

## Summary

When I began my PhD program, Dr. Greg Rieker challenged me with this project. There ended up being a dissertation's worth of science to chew on.
Absorption spectroscopy traditionally measures average gas properties (temperature, composition...) along the path of the laser.
In principle, one can leverage the many light frequencies emitted by certain lasers to determine the spatial variation in temperature along the laser path.
Greg Rieker was familiar with this concept from his PhD. He anticipated that the new dual frequency comb laser he used at his postdoc, with 100,000s of light frequencies, could perform this measurement better than other lasers.

Method | Instrument | Output
---|---|---
Traditional | 2 lasers, 1 path | 1 temperature
Tomography | 2 lasers, many paths | many temperatures
This work | 1 laser (many wavelengths), 1 path | many temperatures

I first studied the mathematics of the problem, then designed and iterated upon a laboratory proof-of-concept, and finally implemented the technique during a field campaign to a test-rig scramjet system.

## Tools

### - Inverse methods
This project was an inverse problem (specifically a Fredholm integral of the 1st kind), so I learned about Inverse theory first from textbooks and then from class.
I implemented singular value decomposition, Tikhonov regularization, and simulated annealing.
I touched upon Bayesian optimal estimation, but did not think of a particularly strong way to implement it.

### - Object-oriented programming in Python
I created an Python code package, NTfit, to extract temperature nonuniformity from a broadband laser transmission spectrum.
This was a good opportunity to learn object-oriented programming, as each fit is packaged, saved, and reloaded as a single object.
I also used some inheritance, but it turned out to not be critical in the design.

### - Dual-frequency comb laser
For the proof-of-concept and the field campaign, I took measurements with a few dual frequency comb laser systems my coworker built.
I also did some minimal optics design around the proof-of-concept furnace system.

### - Heat transfer modelling
For a proof-of-concept of the technique, I needed to compare the laser temperature measurement against some "true" temperature profile.
It became clear that the laboratory system had considerable heat transfer through radiation and advection, making thermocouple measurements inaccurate.
I did minimal finite-element modelling of the system, and mainly dug through literature for an appropriate natural convection model to apply to the geometry of my system.

