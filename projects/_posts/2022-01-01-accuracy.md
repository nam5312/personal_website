---
layout: project
title: accuracy
longtitle: Dual-comb spectroscopy at 0.2% accuracy
era: nist
year: 2021-2024
---

## Background

I did my postdoc at NIST, the National Institute of Standards and Technology,
in a pioneer [group](https://www.nist.gov/ctl/spectrum-technology-and-research-division/fiber-sources-and-applications-group) for fiber dual-comb spectroscopy.
One of the group's research areas is monitoring outdoor concentrations of greenhouse gases (such as carbon dioxide and methane),
by sending infrared frequency-comb laser light out a telescope to a mirror placed kilometers away.

This NIST group pioneered this outdoor measurement technique in 2012,
and have since monitored greenhouse-gas emissions by comparing laser measurements upwind and downwind of a source.
These are relative emasurements, which compares the gas concentrations recorded by the same instrument at two conditions.
In comparison, absolute measurements of gas concentration remain a challenge.

I studied how to measure greenhouse gas concentrations with 0.2% absolute accuracy.
This would enable the dual-comb spectrometer to calibrate other Earth-monitoring instruments such as satellites.
However, to get from percent accuracy down to 0.2%, we must reduce many potential sources of bias:
accurate absorption models, RF signal processing, laser-power corrections, etc.

## Tools

### - Fiber lasers
The core component of the instrument is the erbium-fiber frequency-comb, which at this point is a widely-available commercial product.
However, I had to design many peripherals for this laser, to manipulate the 1565 nm output light to produce a supercontinuum spectrum.
For isntance, to measure atmospheric oxygen I needed to produce milliwatts of frequency-comb light around 1270 nm,
which has historically been a no-light zone of previous dual-comb systems.

### - RF signal processing
The experiment schematic typically shows the laser propagating through a gas to a photodetector, but that's not the whole instrumentation.
The photdetector produces a radio-frequency signal which we have to route to an ADC (analog-to-digitzl converter) and digitize without distorting the signal.
To avoid such distortion, I needed to measure the nonlinear ADC response,
develop a theory of how the ADC response distorts the laser signal,
and design an RF circuit to compoensate for this nonlinearity without adding additional distortion.


