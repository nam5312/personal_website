# personal_website

Professional website--adapted from the [Bedford Lab Group website](https://bedford.io/misc/about/) but want a different font/styling, and also be a bit like Paul Schroeder's website.
These instructions also come from the Bedford README, with some additions I found useful in producing this format.

### Prerequisite software

[Jekyll](http://jekyllrb.com/) to make the website from the source files. They have a great installation guide if you click around from the [Docs](https://jekyllrb.com/docs/) tab.

It's way easier to install Jekyll and its prerequisites with UNIX systems. I did this from Windows, and got stuck on the Command Prompt install until I installed Ubuntu for Windows through the Microsoft Store.
It does take a couple hours to install and update the first time, but then it's robust.

You also need to install Git and get an account on GitLab, GitHub or similar if you want to make the site online rather than just on your local server.

## Build site

To build the website locally, clone the repo with:

```
git clone https://gitlab.com/nam5312/personal_website.git
```

Then install necessary Ruby dependencies by running `bundle install` from within the `personal_website` directory.
This should install everything in the Gemfile onto your local machine.
After this, the site can be be built (into `personal_website\_site`) with:

```
bundle exec jekyll build
```

(If you are getting errors at this stage, it may be due to your version of `bundle`. Try `gem uninstall bundler` + `gem install bundler -v 1.13.1`.)

To view the site, run `bundle exec jekyll serve` and point a browser to `http://localhost:4000/personal_website/`.
Until you escape this command on Bash, any changes you make to the source code should update onto both `personal_website\_site` and the local server.

More information on Jekyll can be found [here](http://jekyllrb.com/).

## Contribute

Blog posts (and papers and projects) just require YAML top matter that looks something like:

```
---
layout: post
title: Newton Institute presentation
author: Trevor Bedford
link: http://www.newton.ac.uk/programmes/IDD/seminars/2013082213301.html
image: /images/blog/transmission.png
---
```

The `layout`, `title` and `author` tags are required, while `link` and `image` are optional.  Just save a Markdown file with this top matter as something like `blog/_posts/2013-08-27-newton-institute.md`, where `2013-08-27` is the date of the post and `newton-institute` is the short title.  This short title is used in the URL of the post, so this becomes `blog/newton-institute/`, so the short title should be long enough and unique enough not to cause conflicts with other posts.
You can check out the `_layouts\paper.html` for all of the `{{ page.___ }}` entries to see what front matter you can add 

### Images

Non-blog pictures are 260x260 pixels. I will crop original photos to square in Windows Photos, then resize to 260x260, finally (if image is over 50-100kB) open in Paint and save as JPEG.

## For more information

* Look over the [metadata format guide](http://bedford.io/guide/format/)
* Look over the [Markdown style guide](http://bedford.io/guide/style/)

## License

All source code in this repository, consisting of files with extensions `.html`, `.css`, `.less`, `.rb` or `.js`, is freely available under an MIT license, unless otherwise noted within a file.
You're welcome to borrow / repurpose code to build your own site, but maybe link to my parent site [bedford.io](http://bedford.io) from your `about` page.

**The MIT License (MIT)**

Copyright (c) 2020 Nathan Malarich

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.



