---
layout: default
title: About
---
This website is designed through [Jekyll](https://jekyllrb.com) and powered through GitLab. 
Thanks to the [Bedford lab](https://bedford.io) for the initial website design. If you like this website design,
check out the Bedford lab's About page and GitHub page for instructions on getting started.

I made a lot of HTML changes to the Bedford design to make the personal website structure rather than the lab group website structure.
I also added some extra icons from [Academicons](https://jpswalsh.github.io/academicons/) to make the paper links look like the [Niemeyer Group website](https://niemeyer-research-group.github.io/papers/paper/swept-heterogeneous).
Finally, I added some ```.gitlab_ci.yml``` and customized the ```_config.yml``` file so the website would load on the GitLab server. There are certainly better ways to format these files, but these worked for me.
If you are looking to make a website, and this is closer to your needs than the Bedford site, you can download or fork the source code from [GitLab](https://gitlab.com/nam5312/personal_website).

The primary purpose of this site is to help you maneuver through my scientific work.
I hope it helps you to see which publications are connected with each other,
and helps you easily access the appropriate data and software with each project.
I often submit preprints of my publications to [ArXiv](https://arxiv.org/search/?query=malarich&searchtype=author&source=header),
and deposit my data and figure scripts on [Zenodo](https://zenodo.org/search?page=1&size=20&q=malarich).
I also sometimes have software packages on [GitLab](https://gitlab.com/nam5312/) that I make public upon release of the paper.


